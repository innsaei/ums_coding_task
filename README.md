# User Management System
This is a simple user management system based on `symfony 5`, `mysql` and `api platform`. 
It allows for dynamic assignment of Roles and Permissions.

Something to consider. If you look into the Permission entity, you will see that there are constants
for each of the actions the user can perform. These permissions are also loadad into the database.
My original idea was to implement `voters` to check if the current user's has that permission available
(inhereted from his ROLE).

Nevertheless due to time constraints I was not able to finish implementing them.

But the endpoints are still secure nonetheless, because only the ROLE_ADMIN has access 
to the /api endpoints in the security.yaml access control section.

## Installation

1. Build the containers
```
docker-compose up -d --build
```

2. If necessary, change the `DATABASE_URL` variable in the `.env` file with this one:
```
DATABASE_URL="mysql://root:root@mysql/ums_coding_task"
```

3. Log into the container
```
docker-compose exec php bash
```

4. Create the database
```
bin/console doctrine:database:create
```

5. Run migrations
```
bin/console doctrine:migrations:migrate
```

6. Run the fixtures to seed the database
```
bin/console doctrine:fixtures:load
```

7. Run these commands to generate public and private keys for signing JWT tokens
```
php bin/console lexik:jwt:generate-keypair
setfacl -R -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
setfacl -dR -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
```

9. You may get a token for each user by executing these commands *outside the docker container*. (needs curl installed)

```
For Admin Role:
---------------
curl -X POST -H "Content-Type: application/json" http://localhost:8001/authentication_token -d '{"email":"admin@ums.com","password":"test"}'
```
```
For User Role:
--------------
curl -X POST -H "Content-Type: application/json" http://localhost:8001/authentication_token -d '{"email":"user@ums.com","password":"test"}'
```


10. Ready to use, you can look into the api doc in `http://127.0.0.1:8001/docs`. You can paste the token in the authorization popup that opens by clicking `Authorize`. But please remember to add the word `Bearer ` before the token, cause it woun't do it by itself.


## Extra documentation

- [UML domain model](docs/ums_model.png) 
- [DB diagram](https://dbdiagram.io/d/6303531ef1a9b01b0fb8d9d6)