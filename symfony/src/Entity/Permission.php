<?php

namespace App\Entity;

use App\Repository\PermissionRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PermissionRepository::class)
 */
class Permission
{
    // User permissions
    public const CREATE_USER = 'create_user';
    public const DELETE_USER = 'delete_user';
    
    // Group permissions
    public const CREATE_GROUP = 'create_group';
    public const DELETE_GROUP = 'delete_group';
    public const ADD_USER_TO_GROUP = 'add_user_to_group';
    public const REMOVE_USER_TO_GROUP = 'remove_user_to_group';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Role::class, inversedBy="permissions")
     */
    private $roles;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
        $this->roles = new ArrayCollection();
    }

    /**
     * Returns a list of all available permissions as defined
     * in the class constants. These should be in synch with DB.
     */
    public static function getPermissionsByRole(): array
    {
        return [
            ROLE::ROLE_USER => [],
            ROLE::ROLE_ADMIN => [
                self::CREATE_USER,
                self::DELETE_USER,
                self::CREATE_GROUP,
                self::DELETE_GROUP,
                self::ADD_USER_TO_GROUP,
                self::REMOVE_USER_TO_GROUP,
            ],
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Role>
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    public function addRole(Role $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole(Role $role): self
    {
        $this->roles->removeElement($role);

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
