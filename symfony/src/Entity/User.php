<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\UserRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User Class
 * 
 * Even though the get url is created by default in api-platform
 * do not remove it from itemOperations below, because it's needed for internal mapping of the resource
 * and many endpoints otherwise start breaking.
 * 
 * @ApiResource(
 *     normalizationContext={"groups"={"base", "user"}},
 *     itemOperations={
 *         "get",
 *         "delete"
 *     }
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @Groups("base", "user")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("base", "user")
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @Groups("user")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @Groups("user")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @Groups("user")
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @Groups("user")
     * This property is not named "groups" because it is a reserved word in symfony
     * @ORM\ManyToMany(targetEntity=Group::class, mappedBy="users")
     */
    private $user_groups;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @Groups("user")
     * @ORM\ManyToMany(targetEntity=Role::class, inversedBy="users")
     */
    private $roles;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->user_groups = new ArrayCollection();
        $this->roles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;
        
        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * Even though in our case Roles is a collection of objects, we must return an array of strings
     * in order for the lexikJWTAuthenticationBundle to work.
     * Probably a better solution is using a compiler pass to change the way the Auth class works, so that it will accept
     * an array of objects, but I don't think it's worth the time for this project.
     */
    public function getRoles(): Array
    {
        $roleNames = [];
        
        foreach ($this->roles as $role) {
            $roleNames[] = $role->getName();
        }

        return $roleNames;
    }

    public function getRolesAsObjects(): Collection
    {
        return $this->roles;
    }

    public function addRole(Role $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * This function does not make much sense, because we have a ManyToMany relationship
     * with Role. But we need to keep it here because the interface forces us to implement it
     * and otherwise the authentication will not work.
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
    
    public function removeRole(Role $role): self
    {
        $this->roles->removeElement($role);

        return $this;
    }
}
