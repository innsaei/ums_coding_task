<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Api\AssignUserToGroupAction;
use App\Controller\Api\RemoveUserFromGroupAction;
use App\Repository\GroupRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"base", "group"}},
 *     collectionOperations = {
 *         "get"
 *     },
 *     itemOperations={
 *         "get",
 *         "delete",
 *         "assign_user" = {
 *             "method" = "PUT",
 *             "path" = "/groups/{groupId}/users/{userId}/assign",
 *             "controller" = AssignUserToGroupAction::class,
 *             "read" = false,
 *             "openapi_context" = {
 *                 "summary" = "Assigns a user to a group",
 *                 "parameters" = {
 *                     {
 *                         "name" = "groupId", "in" = "path", "type" = "integer", "required" = true,
 *                     },
 *                     {
 *                         "name" = "userId", "in" = "path", "type" = "integer", "required" = true,
 *                     },
 *                 },
 *             }
 *         },
 *         "remove_user" = {
 *             "method" = "PUT",
 *             "path" = "/groups/{groupId}/users/{userId}/remove",
 *             "controller" = RemoveUserFromGroupAction::class,
 *             "read" = false,
 *             "openapi_context" = {
 *                 "summary" = "Removes a user from a group",
 *                 "parameters" = {
 *                     {
 *                         "name" = "groupId", "in" = "path", "type" = "integer", "required" = true,
 *                     },
 *                     {
 *                         "name" = "userId", "in" = "path", "type" = "integer", "required" = true,
 *                     },
 *                 },
 *             }
 *         }
 *     }
 * )
 * @ORM\Entity(repositoryClass=GroupRepository::class)
 * @ORM\Table(name="`group`")
 */
class Group
{
    /**
     * @Groups("base", "group")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("base", "group")
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Groups("group")
     * @Groups("user_details")
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="user_groups")
     */
    private $users;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
