<?php

namespace App\DataFixtures;

use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RoleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $adminRole = new Role();
        $adminRole->setName(Role::ROLE_ADMIN);

        $userRole = new Role();
        $userRole->setName(Role::ROLE_USER);
        
        $manager->persist($userRole);
        $manager->persist($adminRole);
        $manager->flush();

        // with these references other fixtures can get these objects 
        // using the Role::ROLE_ADMIN and Role::ROLE_USER constants
        $this->addReference(Role::ROLE_ADMIN, $adminRole);
        $this->addReference(Role::ROLE_USER, $userRole);
    }
}
