<?php

namespace App\DataFixtures;

use App\Entity\Permission;
use App\Repository\RoleRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PermissionFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        foreach (Permission::getPermissionsByRole() as $role => $permissions) {
            foreach ($permissions as $permission) {
                $permissionObj = new Permission();
                $permissionObj->setName($permission);                
                $permissionObj->addRole($this->getReference($role));
                
                $manager->persist($permissionObj);
                $manager->flush();
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RoleFixtures::class,
        ];
    }
}
