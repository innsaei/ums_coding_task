<?php

namespace App\DataFixtures;

use App\Entity\Role;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    private $passwordHasher;
    
    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $adminUser = new User();
        $adminUser->setUsername('Admin');
        $adminUser->setEmail('admin@ums.com');
        $adminUser->setPassword(
            $this->passwordHasher->hashPassword($adminUser, 'test')
        );

        $regularUser = new User();
        $regularUser->setUsername('User');
        $regularUser->setEmail('user@ums.com');
        $regularUser->setPassword(
            $this->passwordHasher->hashPassword($regularUser, 'test')
        );

        // this references return the Role object created in RoleFixtures
        $adminUser->addRole($this->getReference(Role::ROLE_ADMIN));
        $regularUser->addRole($this->getReference(Role::ROLE_USER));
        

        $manager->persist($adminUser);
        $manager->persist($regularUser);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RoleFixtures::class,
        ];
    }
}
