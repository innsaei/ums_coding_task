<?php
 
namespace App\Controller\Api;

use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Group;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class RemoveUserFromGroupAction extends AbstractController
{
    private $em;
    private $userRepository;
    private $groupRepository;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->em = $doctrine->getManager();
        $this->userRepository = $this->em->getRepository(User::class);
        $this->groupRepository = $this->em->getRepository(Group::class);
    }

    public function __invoke(int $groupId, int $userId)
    {
        $user = $this->userRepository->find($userId);
        $group = $this->groupRepository->find($groupId);
        
        if (!$user) {
            throw new NotFoundResourceException('User with id ' . $userId . ' was not found');
        }
 
        if (!$group) {
            throw new NotFoundResourceException('Group with id ' . $groupId . ' was not found');
        }
        
        $group->removeUser($user);
        $this->em->persist($group);
        $this->em->flush();

        return $group;
    }
}