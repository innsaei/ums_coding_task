<?php
 
namespace App\Controller\Api;

use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Group;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Translation\Exception\NotFoundResourceException;


class AssignUserToGroupAction extends AbstractController
{
    private $em;
    private $userRepository;
    private $groupRepository;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->em = $doctrine->getManager();
        $this->userRepository = $this->em->getRepository(User::class);
        $this->groupRepository = $this->em->getRepository(Group::class);
    }

    public function __invoke(int $groupId, int $userId)
    {
        // TODO refactor: the code between AssignUserToGroup and RemoveUserFromGroup is almost identical
        // it would be a good practice to centralize it in something like  a GroupService class
        // that would handle this logic.

        $user = $this->userRepository->find($userId);
        $group = $this->groupRepository->find($groupId);
        
        if (!$user) {
            throw new NotFoundResourceException('User with id ' . $userId . ' was not found');
        }
 
        if (!$group) {
            throw new NotFoundResourceException('Group with id ' . $groupId . ' was not found');
        }
        
        $group->addUser($user);
        $this->em->persist($group);
        $this->em->flush();

        return $group;
    }
}